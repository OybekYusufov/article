package models

type Person struct {
	Firstname string
	Lastname string // Compact by combining the various fields of the same type
}
