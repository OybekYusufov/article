package test

import (
	"bootcamp/article/storage"
	"log"
	"os"
	"testing"
)

var inMemory storage.ArticleStorage

func TestMain(m *testing.M) {
	inMemory = make(storage.ArticleStorage)
	log.Println(inMemory)
	os.Exit(m.Run())
}
