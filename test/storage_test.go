package test

import (
	"bootcamp/article/models"
	"bootcamp/article/storage"
	"testing"
	"time"
	"fmt"
)

func add(a models.Article) error {
	return inMemory.Add(a)
}

func getByID(ID int) (models.Article, error) {
	return inMemory.GetByID(ID)
}

func count() int {
	return len(inMemory)
}

func search(str, tip string) []models.Article{
	return inMemory.Search(str,tip)	
}
func  update( a models.Article) error {
	return inMemory.Update(a)
}
func delete(ID int) error{
	return inMemory.Delete(ID)
}

func TestStorage(t *testing.T) {
	t1 := time.Now()
	a1 := models.Article{
		ID: 1,
		Content: models.Content{
			Title: "Lorem",
			Body:  "Lorem ipsum",
		},
		Author: models.Person{
			Firstname: "John",
			Lastname:  "Doe",
		},
		CreatedAt: &t1,
	}

	//// Positive test case
	c := count()
	err := add(a1)
	if err != nil {
		t.Errorf("Add FAILED: %v", err)
	}
	if count() != c+1 {
		t.Errorf("Add FAILED: expected count %d but got %d", c+1, count())
	}
	////

	//// Negative test case
	err = add(a1)
	if err != storage.ErrorAlreadyExists {
		t.Error("Add FAILED: should return error", storage.ErrorAlreadyExists)
	}
	////

	fmt.Println("Add PASSED")

	res1, err := getByID(a1.ID)
	if err != nil {
		t.Errorf("GetByID FAILED: %v", err)
	}
	if a1 != res1 {
		t.Errorf("Add or GetByID FAILED: expected %v but got %v", a1, res1)
	}

	_, err = getByID(-1)
	if err != storage.ErrorNotFound {
		t.Errorf("Add returns error: %v", err)
	}

	fmt.Println("GetByID PASSED")

	//search

	res:=search("1","ID")
	if len(res)==0{
		t.Errorf("search faild")
	}

	res =search("lorem","enother")
	if len(res)>0{
		t.Errorf("search faild ")
	}

	fmt.Println("SEARCH PASSED")

	//update
	a2 := models.Article{
		ID: 1,
		Content: models.Content{
			Title: "Lorem",
			Body:  "Lorem ipsum",
		},
		Author: models.Person{
			Firstname: "John",
			Lastname:  "Doe",
		},
		CreatedAt: &t1,
	}
    
	up:=update(a2)
	if up!=nil{
		t.Errorf("update faild")
	}

	a2.ID=-1
	up=update(a2)
	if up==nil{
		t.Errorf("update faild")
	}
	fmt.Println("UPDATE PASSED")

	//DELETE 
	del:=delete(1)
	if del!=nil{
		t.Errorf("delete error")
	}

	del=delete(-1)
	if del==nil{
		t.Errorf("delete erors")
	}

	fmt.Println("DELETE PASSED")

}
