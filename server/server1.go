package main

import (
	"bootcamp/article/models"
	"bootcamp/article/storage"
	"strconv"

	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	_ "bootcamp/article/server/docs"
)


var inMemory storage.ArticleStorage

// @title Swagger Example API
// @version 1.1
// @description This is a sample server celler server.
// @termsOfService http://udevs.io/
// @host localhost:7070

func main() {

	gin.SetMode(gin.DebugMode)
	r := gin.New()
	r.Use(gin.Logger(), gin.Recovery())

	inMemory = make(storage.ArticleStorage)

	r.POST("/articles", CreateHandler)
	r.GET("/articles", GetAllHandler)
	r.GET("/articles/byid/:id", GetByIDHandler)
	r.PUT("/articles", UpdateHandler)
	r.DELETE("/articles/:id", DeleteHandler)
	r.PATCH("/articles/search", SearchHandler)

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	r.Run(":7070") // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}

// CreatHandler godoc
// @Description Create Articles
// @ID create-article-handler
// @Summary create a new articles
// @Tags Create
// @Accept json
// @Produce json
// @Param Article body models.Article true "Article"
// @Success 200 {object} models.Article
// @Router /articles [post]
func CreateHandler(c *gin.Context) {
	var a1 models.Article
	err := c.BindJSON(&a1)
	if err != nil {
		c.JSON(400, gin.H{
			"error": err.Error(),
		})
	}
	err = inMemory.Add(a1)
	if err != nil {
		c.JSON(400, gin.H{
			"error": err.Error(),
		})
		return
	}
	c.String(200, "Ok")
}
// GetHandlar godoc
// @Summary add to article
// @Description Show all Article 
// @ID all-article-handler
// @Tags Get ALL
// @Accept  json
// @Produce  json
// @Success 200 {array} models.Article
// @Router /articles [get]
func GetAllHandler(c *gin.Context) {
	resp := inMemory.GetAll()

	c.JSON(200, resp,)
}

// GetByIDHandler godoc
// @Description Get By ID Articles
// @ID id-article-handler
// @Summary get by id articles
// @Tags Get by ID
// @Accept json
// @Produce json
// @Param id path int true "ID"
// @Success 200 {object} models.Article
// @Router /articles/byid/{id} [get]
func GetByIDHandler(c *gin.Context) {
	idStr := c.Param("id")
	idNum, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(400, gin.H{
			"error": err.Error(),
		})
		return
	}
	resp,err:= inMemory.GetByID(int(idNum))
	if err !=nil{
		c.JSON(400, gin.H{
			"error": "Not Faund",
		})
		return
	}

	c.JSON(200, resp)
}

// DeleteHandler godoc
// @Description Delete By ID Articles
// @ID delete-article-handler
// @Summary delete by id articles
// @Tags Delete
// @Accept json
// @Produce json
// @Param id path int true "id"
// @Success 200 {object} models.Article
// @Router /articles/{id} [delete]
func DeleteHandler(c *gin.Context) {
	idStr := c.Param("id")
	idNum, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(400, gin.H{
			"error": err.Error(),
		})
		return
	}
	err= inMemory.Delete(idNum)
	if err != nil {
		c.JSON(400, gin.H{
			"error": err.Error(),
		})
		return
	}
	c.String(200,"ok")
}
// UpdateHandler godoc
// @Description UpdateArticles
// @ID update-article-handler
// @Summary update with id articles
// @Tags Update
// @Accept json
// @Produce json
// @Param Article body models.Article true "Article"
// @Success 200 {object} models.Article
// @Router /articles [put]
func UpdateHandler(c *gin.Context) {
	var a1 models.Article
	err := c.BindJSON(&a1)
	if err != nil {
		c.JSON(400, gin.H{
			"error": err.Error(),
		})
	}
	err = inMemory.Update(a1)
	if err != nil {
		c.JSON(400, gin.H{
			"error": err.Error(),
		})
		return
	}
	c.String(200, "Ok")
}
// SearchHandler godoc
// @Description Search Articles
// @ID search-article-handler
// @Summary search by value with tipe
// @Tags Search
// @Accept json
// @Produce json
// @Param value query string true "value"
// @Param tipe query string true "tipe"
// @Success 200 {object} models.Article
// @Router /articles/search [patch]
func SearchHandler(c *gin.Context){
	value:=c.Query("value")
	tipe:=c.Query("tipe")
	resp:= inMemory.Search(value,tipe)
	c.JSON(200, resp)
}