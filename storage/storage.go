package storage

import (
	"bootcamp/article/models"
	"errors"
	"strconv"
)

var ErrorAlreadyExists error = errors.New("already items")
var ErrorNotFound error = errors.New("not faund")

type ArticleStorage map[int]models.Article

func (storage ArticleStorage) Add(entity models.Article) error {
	if _, ok := storage[entity.ID]; ok {
		return ErrorAlreadyExists
	}
	storage[entity.ID] = entity
	return nil
}

func (storage ArticleStorage) GetByID(ID int) (models.Article, error) {
	var resp models.Article
	if _, of := storage[ID]; !of {
		return resp, ErrorNotFound
	}
	resp = storage[ID]
	return resp, nil
}

func (storage ArticleStorage) GetAll() []models.Article {
	var resp []models.Article
	for v := range storage {
		resp = append(resp, storage[v])
	}
	return resp
}

func (storage ArticleStorage) Search(str, tip string) []models.Article {
	var resp []models.Article
	switch tip {
	case "ID":
		if ID, err := strconv.Atoi(str); err == nil {
			if _, ok := storage[ID]; ok {
				resp = append(resp, storage[ID])
				return resp
			}
		}
		// fallthrough
	case "Title":
		{
			for v := range storage {
				if storage[v].Title == str {
					resp = append(resp, storage[v])
				}
			}
			return resp
		}
		// fallthrough
	case "Body":
		for v := range storage {
			if storage[v].Body == str {
				resp = append(resp, storage[v])
			}
		}
		return resp
		// fallthrough
	case "Firstname":
		for v := range storage {
			if storage[v].Author.Firstname == str {
				resp = append(resp, storage[v])
			}
		}
		return resp
		// fallthrough
	case "Lastname":
		for v := range storage {

			if storage[v].Author.Lastname == str {
				resp = append(resp, storage[v])
			}
		}
		return resp
	default:
		return resp
	}

	return resp
}

func (storage ArticleStorage) Update(entity models.Article) error {
	if _, ok := storage[entity.ID]; !ok {
		return errors.New("error")
	}
	storage[entity.ID] = entity
	return nil
}

func (storage ArticleStorage) Delete(ID int) error {
	if _, ok := storage[ID]; !ok {
		return errors.New("error")
	}
	delete(storage, ID)
	return nil
}
