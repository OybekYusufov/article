package main

import (
	//	"Articles"
	"bootcamp/article/models"
	"bootcamp/article/storage"
	"fmt"
	"time"
)

var inMemory storage.ArticleStorage

func main() {

	inMemory = make(storage.ArticleStorage)
	var a1 models.Article
	a1.ID = 1
	a1.Title = "Lorem"
	a1.Body = "Lorem ipsum"
	var p models.Person = models.Person{
		Firstname: "John",
		Lastname:  "Doe",
	}
	a1.Author = p
	t := time.Now()
	a1.CreatedAt = &t
	err := inMemory.Add(a1)
	if err != nil {
		fmt.Println(err)
	}
	err = inMemory.Add(a1)
	if err != nil {
		fmt.Println(err)
	}
	var a2 models.Article
	a2.ID = 2
	a2.Title = "jack"
	a2.Body = "jack ipsum"
	var k models.Person = models.Person{
		Firstname: "jocer",
		Lastname:  "Doe",
	}
	a2.Author = k
	t = time.Now()
	a2.CreatedAt = &t
	err = inMemory.Add(a2)
	if err != nil {
		fmt.Println(err)
	}
	// err = inMemory.Add(a2)
	// if err != nil {
	// 	fmt.Println(err)
	// }

	// a2,err = inMemory.GetByID(2)
	// if err != nil {
	// 	fmt.Println(err)
	// }else{
	// fmt.Println(a2)
	// }

	// err=inMemory.Delete(1)
	// if err != nil {
	// 	fmt.Println(err)
	// }

	a3:= inMemory.Search("1","ID")
	fmt.Println(a3)

	// a4:= inMemory.Search("John","Firstname")
	// fmt.Println(a4)

	// a5:= inMemory.Search("John","Firstname")
	// fmt.Println(a5)

	// a9:= inMemory.Search("John","Firstname")
	// fmt.Println(a9)

	// a6:= inMemory.Search("John","Firstname")
	// fmt.Println(a6)

	a7:= inMemory.GetAll()
	fmt.Println(a7)

	// fmt.Println(inMemory)
}
